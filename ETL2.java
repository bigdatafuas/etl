import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class ETL2 {
  
  public static void main(String[] args) {
    String text= "Text Test Text Text";
    String[] words = extract(text);
    HashMap<String,Integer> wordcount = transform(words);
    load(wordcount);
  }

  static String[] extract(String text)
  {
    String[] words = text.split(" ");
    return words;
  }

  static HashMap<String,Integer> transform(String[] words)
  {
    HashMap<String,Integer> wordcount = new HashMap<String,Integer>();
    for (String w : words)
    {
        String key = w;
        if (wordcount.containsKey(key)==true)
        {
            Integer count = wordcount.get(key);
            count = count + 1;
            wordcount.put(key, count);
        } 
        else
        {
        wordcount.put(key, 0);
        }
    }
    return wordcount;
  }

  static void load(HashMap<String,Integer> wordcount){
    try {
        FileWriter result = new FileWriter("ETL2_result.txt");
        for(Map.Entry<String, Integer> entry : wordcount.entrySet()) {
            result.write(entry.getKey() + " " + entry.getValue() + System.lineSeparator());
         }
        result.close();
      } catch (IOException e) {
        System.out.println("An error occurred.");
        e.printStackTrace();
      }
  }
}


